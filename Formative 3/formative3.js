let buah_buahan = ["Jeruk", "Mangga", "Pisang", "Kiwi", "Alpukat"];

//for loop
for (let i = 0; i < buah_buahan.length; i++) {
	console.log("Loop: "+buah_buahan[i]);
}

// foreach
buah_buahan.forEach(buah => {
	console.log("Each: "+buah);
});

// for in
for (let buah in buah_buahan) {
	console.log("For In: "+buah_buahan[buah]);
}